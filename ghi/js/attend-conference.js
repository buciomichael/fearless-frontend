window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

        // Here, add the 'd-none' class to the loading icon
        const spanTag = document.getElementById('loading-conference-spinner');
        const addD = spanTag.classList;
        addD.add('d-none')
        // Here, remove the 'd-none' class from the select tag
        const removeD = selectTag.classList;
        removeD.remove('d-none')
    }

    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag); // grabs all the html form data and puts into a dictionary for us
      const json = JSON.stringify(Object.fromEntries(formData)); // converting to JSON string
      console.log(json);

      const locationUrl = 'http://localhost:8001/api/attendees/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
  },
};
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        // remove the d-none from the success alert
        const divTag = document.getElementById('success-message')
        const removeNone = divTag.classList;
        removeNone.remove('d-none')
        // add d-none to the form
        const addNone = formTag.classList;
        addNone.add('d-none')
}
    });

  });
