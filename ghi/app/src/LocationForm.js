import React, {useEffect, useState} from 'react';

function LocationForm(props) {
   const [states, setStates] = useState([]);

  // Set the useState hook to store "name" in the component's state,
  // with a default initial value of an empty string.
  const [name, setName] = useState('');

  // Create the handleNameChange method to take what the user inputs
  // into the form and store it in the state's "name" variable.
  function handleNameChange(event) {
    setName(event.target.value);
  }

  const [roomCount, setRoomCount] = useState('');
  function handleRoomCountChange(event) {
    setRoomCount(event.target.value)
  }

  const [city, setCity] = useState('');
  function handleCityChange(event) {
    setCity(event.target.value)
  }

  const [state, setState] = useState('');
  function handleStateChange(event) {
    setState(event.target.value);
   }

   const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.room_count = roomCount;
        data.name = name;
        data.city = city; // these names are coming from the variables in useState(), you should have named them something else lol
        data.state = state; // these names are coming from the variables in useState(), you should have named them something else lol
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                const newLocation = await response.json();
                console.log(newLocation);

                setName('');
                setRoomCount('');
                setCity('');
                setState('');
        }
        }


  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      setStates(data.states)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);


    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-htmlForm">
              <div className="htmlForm-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="Form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="htmlForm-floating mb-3">
                <input value={roomCount} onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="Form-control" />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="htmlForm-floating mb-3">
                <input value={city} onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="Form-control" />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select value={state} onChange={handleStateChange} required name="state" id="state" className="Form-select">
                  <option>Choose a state</option>
                  {states.map(state => {
                    return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                        );
  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

export default LocationForm;
