import React, {useEffect, useState} from 'react';


function ConferenceForm () {
    const [locations, setLocations] = useState([]);

    // Set the useState hook to store "name" in the component's state,
    // with a default initial value of an empty string.
    const [name, setName] = useState('');

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    function handleNameChange(event) {
        setName(event.target.value);
     }

    const [starts, setStarts] = useState('');
    function handleStartsChange(event) {
        setStarts(event.target.value);
    }

    const [ends, setEnds] = useState('');
    function handleEndsChange(event) {
        setEnds(event.target.value);
    }

    const [description, setDescription] = useState('');
    function handleDescriptionChange(event) {
        setDescription(event.target.value);
    }

    const [maxPresentations, setMaxPresentations] = useState('');
    function handlePresentationChange(event) {
        setMaxPresentations(event.target.value);
    }

    const [maxAttendees, setMaxAttendees] = useState('');
    function handleAttendeesChange(event) {
        setMaxAttendees(event.target.value);
    }

    const [location, setLocation] = useState('');
    function handleLocationChange(event) {
        setLocation(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                const newConference = await response.json();
                console.log(newConference);

                setName('');
                setStarts('');
                setEnds('');
                setDescription('');
                setMaxAttendees('');
                setMaxPresentations('');
                setLocation('');
        }
        }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
            setLocations(data.locations)
        }
      }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="htmlForm-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="htmlForm-floating mb-3">
                <input value={starts} onChange={handleStartsChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="htmlForm-floating mb-3">
                <input value={ends} onChange={handleEndsChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="end">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="examplehtmlFormControlTextarea1" className="htmlForm-label">Description</label>
                <textarea value={description} onChange={handleDescriptionChange} className="htmlForm-control" required name="description" id="description" rows="3"></textarea>
              </div>
              <div className="htmlForm-floating mb-3">
                <input value={maxPresentations} onChange={handlePresentationChange} placeholder="Maximum Presentations" required type="text" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="htmlForm-floating mb-3">
                <input value={maxAttendees} onChange={handleAttendeesChange} placeholder="Maximum Attendees" required type="text" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
